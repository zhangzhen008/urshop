﻿using Microsoft.AspNetCore.Http;

namespace Urs.Framework.Mvc
{
    public partial class BaseModel
    { 
        public IFormCollection Form { get; set; }
    }
}
