﻿using FluentValidation;
using Urs.Admin.Models.Topics;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Topics
{
    public class TopicValidator : BaseUrsValidator<TopicModel>
    {
        public TopicValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.SystemName).NotNull().WithMessage(localizationService.GetResource("Admin.ContentManagement.Topics.Fields.SystemName.Required"));
        }
    }
}