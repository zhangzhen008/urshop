﻿using System.ComponentModel;
using Urs.Data.Domain.Agents;
using Urs.Data.Domain.Orders;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class AgentSettingsModel : BaseModel, ISettingsModel
    {
        /// <summary>
        /// 分红一级佣金比率
        /// </summary>
        public decimal Rate { get; set; }
        /// <summary>
        /// 分红二级佣金比率
        /// </summary>
        public decimal ParentRate { get; set; }
        /// <summary>
        /// 佣金计算方式
        /// </summary>
        [DisplayName("佣金计算方式")]
        public AgentBonusMethod Method { get; set; }
        /// <summary>
        /// 佣金兑换状态
        /// </summary>
        [DisplayName("佣金兑换状态")]
        public OrderStatus BonusForHandout_Awarded { get; set; }
        /// <summary>
        /// 取消状态
        /// </summary>
        [DisplayName("取消状态")]
        public OrderStatus BonusForHandout_Canceled { get; set; }
        /// <summary>
        /// 二级分销启用
        /// </summary>
        [DisplayName("二级分销启用")]
        public bool ParentAgentEnabled { get; set; }
    }
}