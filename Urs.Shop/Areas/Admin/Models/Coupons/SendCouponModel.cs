﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Coupons
{
    public class SendCouponModel: BaseEntityModel
    {

        public SendCouponModel()
        {
            CouponList = new List<CouponModel>();
            List = new List<SelectListItem>();
        }
        /// <summary>
        /// 消费次数
        /// </summary>
        public int ConsumnCount { get; set; }
       /// <summary>
       /// 消费金额
       /// </summary>
        public decimal ConsumnMoney { get; set; }
        /// <summary>
        /// 邀请人数
        /// </summary>
        public int InviteesCount { get; set; }
        public string PictureUrl { get; set; }

        public string Username { get; set; }
        //拥有的优惠卷数量
        public int CouponAmount { get; set; }

        public IList<CouponModel> CouponList { get; set; }

        public IList<SelectListItem> List { get; set; }
    }
}