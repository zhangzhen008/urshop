using Urs.Framework.Models;

namespace Urs.Admin.Models.Agents
{
    public partial class AgentGoodsModel : BaseEntityModel
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string Price { get; set; }

        public decimal ParentRate { get; set; }

        public decimal Rate { get; set; }
    }
}

