﻿using Urs.Framework;
using Urs.Framework.Kendoui;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Stores
{
    public partial class BrandListModel : BaseModel
    {
        [UrsDisplayName("Admin.Store.Brands.List.SearchBrandName")]
        
        public string SearchName { get; set; }
    }
}