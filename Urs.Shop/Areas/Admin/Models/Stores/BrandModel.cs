﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Admin.Models.Users;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Kendoui;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(BrandValidator))]
    public partial class BrandModel : BaseEntityModel
    {
        public BrandModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }
        }

        [UrsDisplayName("Admin.Store.Brands.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.Description")]
        
        public string Description { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.ShowOnPublicPage")]
        public bool ShowOnPublicPage { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.SeName")]
        
        public string SeName { get; set; }

        [UIHint("Picture")]
        [UrsDisplayName("Admin.Store.Brands.Fields.Picture")]
        public int PictureId { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.PageSize")]
        public int PageSize { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.PriceRanges")]
        public string PriceRanges { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.Published")]
        public bool Published { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.Deleted")]
        public bool Deleted { get; set; }

        [UrsDisplayName("Admin.Store.Brands.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        //ACL
        [UrsDisplayName("Admin.Store.Brands.Fields.SubjectToAcl")]
        public bool SubjectToAcl { get; set; }
        [UrsDisplayName("Admin.Store.Brands.Fields.AclUserRoles")]
        public List<UserRoleModel> AvailableUserRoles { get; set; }
        public int[] SelectedUserRoleIds { get; set; }


        #region Nested classes

        public partial class BrandGoodsModel : BaseEntityModel
        {
            public int BrandId { get; set; }

            public int GoodsId { get; set; }

            [UrsDisplayName("Admin.Store.Brands.Goodss.Fields.Goods")]
            public string GoodsName { get; set; }

            [UrsDisplayName("Admin.Store.Brands.Goodss.Fields.IsFeaturedGoods")]
            public bool IsFeaturedGoods { get; set; }

            [UrsDisplayName("Admin.Store.Brands.Goodss.Fields.DisplayOrder")]
            //we don't name it DisplayOrder because Telerik has a small bug 
            //"if we have one more editor with the same name on a page, it doesn't allow editing"
            //in our case it's category.DisplayOrder
            public int DisplayOrder1 { get; set; }
        }

        public partial class AddBrandGoodsModel : BaseModel
        {
            public AddBrandGoodsModel()
            {
                AvailableCategories = new List<SelectListItem>();
                AvailableBrands = new List<SelectListItem>();
            }
            public ResponseResult Goodss { get; set; }

            [UrsDisplayName("Admin.Store.Goods.List.SearchGoodsName")]
            
            public string SearchGoodsName { get; set; }
            [UrsDisplayName("Admin.Store.Goods.List.SearchCategory")]
            public int SearchCategoryId { get; set; }
            [UrsDisplayName("Admin.Store.Goods.List.SearchBrand")]
            public int SearchBrandId { get; set; }

            public IList<SelectListItem> AvailableCategories { get; set; }
            public IList<SelectListItem> AvailableBrands { get; set; }

            public int BrandId { get; set; }

            public int[] SelectedGoodsIds { get; set; }
        }

        #endregion
    }
}