﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Urs.Admin.Models.Common;
using Urs.Core.Plugins;
using Urs.Data.Domain.Configuration;
using Urs.Services.Configuration;
using Urs.Services.Plugins;
using Urs.Services.Security;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Framework.Kendoui;
using Urs.Framework.Mvc;

namespace Urs.Admin.Controllers
{
    [AdminAuthorize]
    public partial class WidgetController : BaseAdminController
	{
		#region Fields

        private readonly IWidgetService _widgetService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly WidgetSettings _widgetSettings;
	    private readonly IPluginFinder _pluginFinder;

	    #endregion

		#region Constructors

        public WidgetController(IWidgetService widgetService,
            IPermissionService permissionService, ISettingService settingService,
            WidgetSettings widgetSettings, IPluginFinder pluginFinder)
		{
            this._widgetService = widgetService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._widgetSettings = widgetSettings;
            this._pluginFinder = pluginFinder;
		}

		#endregion 
        
        #region Methods
        
        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return HttpUnauthorized();

            return View();
        }

        [HttpPost]
        public IActionResult List(PageRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return HttpUnauthorized();

            var widgetsModel = new List<WidgetModel>();
            var widgets = _widgetService.LoadAllWidgets();
            foreach (var widget in widgets)
            {
                var tmp1 = widget.ToPluginModel<WidgetModel>();
                tmp1.IsActive = widget.IsWidgetActive(_widgetSettings);
                tmp1.ConfigurationUrl = widget.GetConfigurationPageUrl();

                widgetsModel.Add(tmp1);
            }
            var result = new ResponseResult
            {
                data = widgetsModel,
                count = widgetsModel.Count()
            };
            return Json(result);
        }

        [HttpPost]
        public IActionResult WidgetUpdate(WidgetModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return HttpUnauthorized();

            var widget = _widgetService.LoadWidgetBySystemName(model.SystemName);
            if (widget.IsWidgetActive(_widgetSettings))
            {
                if (!model.IsActive)
                {
                    //mark as disabled
                    _widgetSettings.ActiveWidgetSystemNames.Remove(widget.PluginDescriptor.SystemName);
                    _settingService.SaveSetting(_widgetSettings);
                }
            }
            else
            {
                if (model.IsActive)
                {
                    //mark as active
                    _widgetSettings.ActiveWidgetSystemNames.Add(widget.PluginDescriptor.SystemName);
                    _settingService.SaveSetting(_widgetSettings);
                }
            }
            var pluginDescriptor = widget.PluginDescriptor;
            //display order
            pluginDescriptor.DisplayOrder = model.DisplayOrder;
            PluginManager.SavePluginDescriptor(pluginDescriptor);
            //reset plugin cache
            _pluginFinder.ReloadPlugins(pluginDescriptor);

            return new NullJsonResult();
        }
        
        #endregion
    }
}
