﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Common
{
    /// <summary>
    /// 省市区
    /// </summary>
    public partial class MoProvinces
    {
        public MoProvinces()
        {
            Cities = new List<MoCity>();
        }
        /// <summary>
        /// 省名
        /// </summary>
        public string ProvinceName { get; set; }
        /// <summary>
        /// 省名数组
        /// </summary>
        public IList<MoCity> Cities { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public partial class MoCity
        {
            public MoCity()
            {
                Areas = new List<string>();
            }
            /// <summary>
            /// 城市名
            /// </summary>
            public string CityName { get; set; }
            /// <summary>
            /// 区域名称
            /// </summary>
            public IList<string> Areas { get; set; }
        }
    }
}