﻿using System.Collections.Generic;
using Plugin.Api.Models.UserAddress;
using Plugin.Api.Models.ShoppingCart;

namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 提交结算
    /// </summary>
    public class MoCheckoutRequest
    {
        public MoCheckoutRequest()
        {
            Form = new List<MoKeyValue>();
        }
        /// <summary>
        /// 支付方法：微信小程序： Payments.WeixinOpen  微信端微信支付：Payments.WeixinWap
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// 配送方法
        /// </summary>
        public string ShippingMethod { get; set; }
        /// <summary>
        /// 配送地址(二选一)
        /// </summary>
        public int AddressId { get; set; }
        /// <summary>
        /// 自提点Id(二选一)
        /// </summary>
        public int PickupPointId { get; set; }
        /// <summary>
        /// 优惠券Id
        /// </summary>
        public int CouponId { get; set; }
        /// <summary>
        /// 余额付款
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// 收货地址
        /// </summary>
        public MoAddress Address { get; set; }
        /// <summary>
        /// 订单备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 提交结算属性
        /// </summary>
        public IList<MoKeyValue> Form { get; set; }
    }

}