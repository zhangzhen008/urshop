﻿namespace Urs.Plugin.Misc.Api.Models.Agents
{
    /// <summary>
    /// 代理升级信息
    /// </summary>
    public partial class UpgradeInfo
    {
        /// <summary>
        /// 代理邀请码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 升级信息
        /// </summary>
        public string Message { get; set; }
    }
}