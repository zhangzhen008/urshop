﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Plugin.Shipping.ByWeight.Models
{
    public class ShippingByWeightListModel : BaseModel
    {
        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.LimitMethodsToCreated")]
        public bool LimitMethodsToCreated { get; set; }
        
    }
}