using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Mapping;
using Urs.Plugin.Shipping.ByWeight.Domain;

namespace Urs.Plugin.Shipping.ByWeight.Data
{
    /// <summary>
    /// Represents a shipping by weight or by total record mapping configuration
    /// </summary>
    public partial class ShippingByWeightRecordMap : UrsEntityTypeConfiguration<ShippingByWeightRecord>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<ShippingByWeightRecord> builder)
        {
            builder.ToTable(nameof(ShippingByWeightRecord));
            builder.HasKey(record => record.Id);
            builder.Property(pv => pv.First).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.FirstPrice).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.Plus).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.PlusPrice).HasColumnType("decimal(18, 4)");
        }

        #endregion
    }
}