using Urs.Core;
namespace Urs.Data.Domain.Stores
{
    public partial class GoodsTagMapping : BaseEntity
    {
        public int GoodsId { get; set; }
        public int GoodsTagId { get; set; }
        public virtual Goods Goods { get; set; }
        public virtual GoodsTag GoodsTag { get; set; }
    }
}
