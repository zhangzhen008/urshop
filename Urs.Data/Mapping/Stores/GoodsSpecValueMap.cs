
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsSpecValueMap : UrsEntityTypeConfiguration<GoodsSpecValue>
    {
        public override void Configure(EntityTypeBuilder<GoodsSpecValue> builder)
        {
            builder.ToTable(nameof(GoodsSpecValue));
            builder.HasKey(pvav => pvav.Id);
            builder.Property(pvav => pvav.Name).IsRequired().HasMaxLength(400);

            base.Configure(builder);
        }
    }
}