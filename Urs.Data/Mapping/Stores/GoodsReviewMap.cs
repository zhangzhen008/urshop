
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsReviewMap : UrsEntityTypeConfiguration<GoodsReview>
    {
        public override void Configure(EntityTypeBuilder<GoodsReview> builder)
        {
            builder.ToTable(nameof(GoodsReview));
            builder.HasKey(o => o.Id);
            builder.Property(pr => pr.Title);
            builder.Property(pr => pr.ReviewText);

            base.Configure(builder);
        }
    }
}