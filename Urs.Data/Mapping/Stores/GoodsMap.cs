
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsMap : UrsEntityTypeConfiguration<Goods>
    {
        public override void Configure(EntityTypeBuilder<Goods> builder)
        {
            builder.ToTable(nameof(Goods));
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name).IsRequired().HasMaxLength(400);
            builder.Property(p => p.ShortDescription);
            builder.Property(p => p.FullDescription);
            builder.Property(p => p.AdminComment);
            builder.Property(pv => pv.Sku).HasMaxLength(400);
            builder.Property(pv => pv.AdminComment);
            builder.Property(pv => pv.ManufacturerPartNumber).HasMaxLength(400);
            builder.Property(pv => pv.AdditionalShippingCharge).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.Price).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.OldPrice).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.SpecialPrice).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.Weight).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.Length).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.Width).HasColumnType("decimal(18, 4)");
            builder.Property(pv => pv.Height).HasColumnType("decimal(18, 4)");
            base.Configure(builder);
        }
    }
}