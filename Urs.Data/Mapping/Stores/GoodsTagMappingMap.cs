
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsTagMappingMap : UrsEntityTypeConfiguration<GoodsTagMapping>
    {
        public override void Configure(EntityTypeBuilder<GoodsTagMapping> builder)
        {
            builder.ToTable(nameof(GoodsTagMapping));
            builder.HasKey(mapping => new { mapping.GoodsId, mapping.GoodsTagId });

            builder.HasOne(mapping => mapping.Goods)
                .WithMany(goods => goods.GoodsTags)
                .HasForeignKey(mapping => mapping.GoodsId)
                .IsRequired();

            builder.HasOne(mapping => mapping.GoodsTag)
                .WithMany(goodsTag => goodsTag.Goods)
                .HasForeignKey(mapping => mapping.GoodsTagId)
                .IsRequired();

            builder.Ignore(mapping => mapping.Id);
            base.Configure(builder);
        }
    }
}