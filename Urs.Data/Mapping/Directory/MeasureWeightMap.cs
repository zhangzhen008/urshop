
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Directory;

namespace Urs.Data.Mapping.Directory
{
    public partial class MeasureWeightMap : UrsEntityTypeConfiguration<MeasureWeight>
    {
        public override void Configure(EntityTypeBuilder<MeasureWeight> builder)
        {
            builder.ToTable(nameof(MeasureWeight));
            builder.HasKey(m => m.Id);
            builder.Property(m => m.Name).IsRequired().HasMaxLength(100);
            builder.Property(m => m.SystemKeyword).IsRequired().HasMaxLength(100);
            builder.Property(m => m.Ratio).HasColumnType("decimal(18, 8)");
            base.Configure(builder);
        }
    }
}