﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Mapping.Orders
{
    public partial class ShoppingCartItemMap : UrsEntityTypeConfiguration<ShoppingCartItem>
    {
        public override void Configure(EntityTypeBuilder<ShoppingCartItem> builder)
        {
            builder.ToTable(nameof(ShoppingCartItem));
            builder.HasKey(sci => sci.Id);

            builder.Ignore(sci => sci.IsFreeShipping);
            builder.Ignore(sci => sci.IsShipEnabled);
            builder.Ignore(sci => sci.AdditionalShippingCharge);

            builder.HasOne(sci => sci.User)
                .WithMany(c => c.ShoppingCartItems)
                .HasForeignKey(sci => sci.UserId);

            builder.HasOne(sci => sci.Goods)
                .WithMany()
                .HasForeignKey(sci => sci.GoodsId);
            base.Configure(builder);
        }
    }
}
