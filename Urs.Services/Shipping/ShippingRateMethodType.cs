
namespace Urs.Services.Shipping
{
    public enum ShippingRateMethodType : int
    {
        Unknown = 0,
        Offline = 10,
        Realtime = 20
    }
}
