using System;
using System.Collections.Generic;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Payments
{
    [Serializable]
    public partial class ProcessPaymentRequest
    {
        public ProcessPaymentRequest()
        {
            this.CustomValues = new Dictionary<string, object>();
            this.GoodsValues = new Dictionary<string, int>();
        }

        public int UserId { get; set; }
        public Guid OrderGuid { get; set; }
        public decimal OrderTotal { get; set; }
        public string PaymentMethod { get; set; }
        public string ShippingMethod { get; set; }
        public int AddressId { get; set; }
        public ProcessAddressRequest Shipping { get; set; }
        public string Remark { get; set; }
        public int CouponId { get; set; }
        public int PTUserId { get; set; }
        public decimal Balance { get; set; }
        public bool RedeemedPoint { get; set; }
        public string CheckoutAttributeDescription { get; set; }
        public string CheckoutAttributesXml { get; set; }
        public Dictionary<string, object> CustomValues { get; set; }
        public Dictionary<string, int> GoodsValues { get; set; }
    }
    public class ProcessAddressRequest
    {
        public virtual string Name { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Company { get; set; }
        public virtual string ProvinceName { get; set; }
        public virtual string CityName { get; set; }
        public virtual string AreaName { get; set; }
        public virtual string Address { get; set; }
        public virtual string Zip { get; set; }
    }
}
