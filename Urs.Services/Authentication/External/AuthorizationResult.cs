using System;
using System.Collections.Generic;
using Urs.Data.Domain.Users;

namespace Urs.Services.Authentication.External
{
    public partial class AuthorizationResult
    {
        public AuthorizationResult(OpenAuthenticationStatus status)
        {
            this.Errors = new List<string>();
            Status = status;
        }

        public string Token { get; set; }
        public string OpenId { get; set; }
        public User user { get; set; }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }

        public bool Success
        {
            get { return this.Errors.Count == 0; }
        }

        public OpenAuthenticationStatus Status { get; private set; }

        public IList<string> Errors { get; set; }
    }
}