
using System;

namespace Urs.Services.Authentication.External
{
    [Serializable]
    public partial class UserClaims
    {
        public bool IsSignedByProvider { get; set; }
        public Version Version { get; set; }
        public string Nickname { get; set; }
        public string Sex { get; set; }
        public string HeadImgUrl { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
    }

}