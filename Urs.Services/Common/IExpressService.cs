﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urs.Services.Common
{
    public interface IExpressService
    {
        string GetShipmentInfo(string company, string shipmentNumber);
    }
}
