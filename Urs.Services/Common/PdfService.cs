﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Urs.Core;
using Urs.Data.Domain;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Directory;
using Urs.Data.Domain.Localization;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Shipping;
using Urs.Core.Html;
using Urs.Services.Stores;
using Urs.Services.Directory;

using Urs.Services.Localization;
using Urs.Services.Media;
using Urs.Services.Orders;
using Urs.Services.Payments;
using System.Globalization;
using Urs.Core.Infrastructure;
using Urs.Data.Domain.Configuration;

namespace Urs.Services.Common
{
    public partial class PdfService : IPdfService
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IOrderService _orderService;
        private readonly IPaymentService _paymentService;
        private readonly IMeasureService _measureService;
        private readonly IPictureService _pictureService;
        private readonly IGoodsService _goodsService;
        private readonly IGoodsSpecParser _goodsSpecParser;
        private readonly IWebHelper _webHelper;
        private readonly StoreSettings _storeSettings;
        private readonly MeasureSettings _measureSettings;
        private readonly PdfSettings _pdfSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly AddressSettings _addressSettings;
        private readonly IUrsFileProvider _fileProvider;

        #endregion

        #region Ctor

        public PdfService(ILocalizationService localizationService, IOrderService orderService,
            IPaymentService paymentService,
            IMeasureService measureService,
            IPictureService pictureService, IGoodsService goodsService,
            IGoodsSpecParser goodsSpecParser, IWebHelper webHelper,
            StoreSettings storeSettings,
            MeasureSettings measureSettings, PdfSettings pdfSettings, IUrsFileProvider fileProvider,
            StoreInformationSettings storeInformationSettings, AddressSettings addressSettings)
        {
            this._localizationService = localizationService;
            this._orderService = orderService;
            this._paymentService = paymentService;
            this._measureService = measureService;
            this._pictureService = pictureService;
            this._goodsService = goodsService;
            this._goodsSpecParser = goodsSpecParser;
            this._webHelper = webHelper;
            this._storeSettings = storeSettings;
            this._measureSettings = measureSettings;
            this._pdfSettings = pdfSettings;
            this._storeInformationSettings = storeInformationSettings;
            this._addressSettings = addressSettings;
            this._fileProvider = fileProvider;
        }

        #endregion

        #region Utilities

        protected virtual Font GetFont()
        {
            return GetFont(_pdfSettings.FontFileName);
        }
        protected virtual Font GetFont(string fontFileName)
        {
            if (fontFileName == null)
                throw new ArgumentNullException(nameof(fontFileName));

            var fontPath = _fileProvider.Combine(_fileProvider.MapPath("~/App_Data/Pdf/"), fontFileName);
            var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 10, Font.NORMAL);
            return font;
        }
        #endregion

        #region Methods

        public virtual void PrintOrdersToPdf(Stream stream, IList<Order> orders)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (orders == null)
                throw new ArgumentNullException("orders");

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.Letter;
            }

            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, stream);
            doc.Open();

            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.Black;
            var font = GetFont();
            var attributesFont = GetFont();
            attributesFont.SetStyle(Font.ITALIC);

            int ordCount = orders.Count;
            int ordNum = 0;

            foreach (var order in orders)
            {
                #region Header

                var logoPicture = _pictureService.GetPictureById(_pdfSettings.LogoPictureId);
                var logoExists = logoPicture != null;

                var headerTable = new PdfPTable(logoExists ? 2 : 1);
                headerTable.WidthPercentage = 100f;
                if (logoExists)
                    headerTable.SetWidths(new[] { 50, 50 });

                if (logoExists)
                {
                    var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 0, false);
                    var cellLogo = new PdfPCell(Image.GetInstance(logoFilePath));
                    cellLogo.Border = Rectangle.NO_BORDER;
                    headerTable.AddCell(cellLogo);
                }
                var cell = new PdfPCell();
                cell.Border = Rectangle.NO_BORDER;
                cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Order#"), order.Id), titleFont));
                var anchor = new Anchor(_storeInformationSettings.StoreUrl.Trim(new char[] { '/' }), font);
                anchor.Reference = _storeInformationSettings.StoreUrl;
                cell.AddElement(new Paragraph(anchor));
                cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.OrderDate"), order.CreateTime.ToString()), font));
                headerTable.AddCell(cell);
                doc.Add(headerTable);

                #endregion

                #region Addresses

                var addressTable = new PdfPTable(2);
                addressTable.WidthPercentage = 100f;
                addressTable.SetWidths(new[] { 50, 50 });

                cell = new PdfPCell();
                cell.Border = Rectangle.NO_BORDER;

                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                string paymentMethodStr = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService) : order.PaymentMethodSystemName;
                if (!String.IsNullOrEmpty(paymentMethodStr))
                {
                    cell.AddElement(new Paragraph(" "));
                    cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.PaymentMethod"), paymentMethodStr), font));
                    cell.AddElement(new Paragraph());
                }

                addressTable.AddCell(cell);

                if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
                {
                    if (order.ShippingAddress == null)
                        throw new UrsException(string.Format("Shipping is required, but address is not available. Order ID = {0}", order.Id));
                    cell = new PdfPCell();
                    cell.Border = Rectangle.NO_BORDER;

                    cell.AddElement(new Paragraph(_localizationService.GetResource("PDFInvoice.ShippingInformation"), titleFont));
                    if (!String.IsNullOrEmpty(order.ShippingAddress.Company))
                        cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.Company"), order.ShippingAddress.Company), font));
                    cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.Name"), order.ShippingAddress.Name), font));
                    if (_addressSettings.PhoneEnabled)
                        cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.Phone"), order.ShippingAddress.PhoneNumber), font));
                    if (_addressSettings.FaxEnabled && !String.IsNullOrEmpty(order.ShippingAddress.FaxNumber))
                        cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.Fax"), order.ShippingAddress.FaxNumber), font));
                    if (_addressSettings.StreetAddressEnabled)
                        cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.Address"), order.ShippingAddress.Address1), font));
                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(order.ShippingAddress.Address2))
                        cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.Address2"), order.ShippingAddress.Address2), font));
                    if (_addressSettings.ProvincesEnabled || _addressSettings.ZipPostalCodeEnabled)
                        cell.AddElement(new Paragraph("   " + String.Format("{0}, {1} {2}",
                            order.ShippingAddress.ProvinceName, order.ShippingAddress.CityName, order.ShippingAddress.AreaName), font));

                    cell.AddElement(new Paragraph(" "));
                    cell.AddElement(new Paragraph("   " + String.Format(_localizationService.GetResource("PDFInvoice.ShippingMethod"), order.ShippingMethod), font));
                    cell.AddElement(new Paragraph());

                    addressTable.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Phrase(" "));
                    cell.Border = Rectangle.NO_BORDER;
                    addressTable.AddCell(cell);
                }

                doc.Add(addressTable);
                doc.Add(new Paragraph(" "));

                #endregion

                #region Goodss
                doc.Add(new Paragraph(_localizationService.GetResource("PDFInvoice.Goods(s)"), titleFont));
                doc.Add(new Paragraph(" "));


                var orderGoodsVariants = _orderService.GetAllorderItems(order.Id, null, null, null, null, null, null);

                var goodsTable = new PdfPTable(_storeSettings.ShowGoodsSku ? 5 : 4);
                goodsTable.WidthPercentage = 100f;
                goodsTable.SetWidths(_storeSettings.ShowGoodsSku ? new[] { 40, 15, 15, 15, 15 } : new[] { 40, 20, 20, 20 });

                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.GoodsName"), font));
                cell.BackgroundColor = BaseColor.LightGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                goodsTable.AddCell(cell);

                if (_storeSettings.ShowGoodsSku)
                {
                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.SKU"), font));
                    cell.BackgroundColor = BaseColor.LightGray;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    goodsTable.AddCell(cell);
                }

                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.GoodsPrice"), font));
                cell.BackgroundColor = BaseColor.LightGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                goodsTable.AddCell(cell);

                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.GoodsQuantity"), font));
                cell.BackgroundColor = BaseColor.LightGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                goodsTable.AddCell(cell);

                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.GoodsTotal"), font));
                cell.BackgroundColor = BaseColor.LightGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                goodsTable.AddCell(cell);

                for (int i = 0; i < orderGoodsVariants.Count; i++)
                {
                    var orderGoodsVariant = orderGoodsVariants[i];

                    string name = orderGoodsVariant.GoodsName;
                    cell = new PdfPCell();
                    cell.AddElement(new Paragraph(name, font));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(orderGoodsVariant.AttributeDescription, true, true), attributesFont);
                    cell.AddElement(attributesParagraph);
                    goodsTable.AddCell(cell);

                    if (_storeSettings.ShowGoodsSku)
                    {
                        var sku = orderGoodsVariant.Goods.FormatSku(orderGoodsVariant.AttributesXml, _goodsSpecParser);
                        cell = new PdfPCell(new Phrase(sku ?? String.Empty, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        goodsTable.AddCell(cell);
                    }

                    string unitPrice = PriceFormatter.FormatPrice(orderGoodsVariant.UnitPrice);

                    cell = new PdfPCell(new Phrase(unitPrice, font));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    goodsTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(orderGoodsVariant.Quantity.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    goodsTable.AddCell(cell);

                    string subTotal = PriceFormatter.FormatPrice(orderGoodsVariant.Price);

                    cell = new PdfPCell(new Phrase(subTotal, font));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    goodsTable.AddCell(cell);
                }
                doc.Add(goodsTable);

                #endregion

                #region Checkout attributes

                if (!String.IsNullOrEmpty(order.CheckoutAttributeDescription))
                {
                    doc.Add(new Paragraph(" "));
                    string attributes = HtmlHelper.ConvertHtmlToPlainText(order.CheckoutAttributeDescription, true, true);
                    var pCheckoutAttributes = new Paragraph(attributes, font);
                    pCheckoutAttributes.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(pCheckoutAttributes);
                    doc.Add(new Paragraph(" "));
                }

                #endregion

                #region Totals

                doc.Add(new Paragraph(" "));

                var orderSubtotalExclTaxInUserCurrency = order.OrderSubtotal;
                string orderSubtotalExclTaxStr = PriceFormatter.FormatPrice(orderSubtotalExclTaxInUserCurrency);

                var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Sub-Total"), orderSubtotalExclTaxStr), font);
                p.Alignment = Element.ALIGN_RIGHT;
                doc.Add(p);

                if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
                {

                    var orderShippingExclTaxInUserCurrency = order.OrderShipping;
                    string orderShippingExclTaxStr = PriceFormatter.FormatPrice(orderShippingExclTaxInUserCurrency);

                    var par = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Shipping"), orderShippingExclTaxStr), font);
                    p.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(par);

                }

                if (order.PaymentMethodAdditionalFee > decimal.Zero)
                {
                    var paymentMethodAdditionalFee = order.PaymentMethodAdditionalFee;
                    string paymentMethodAdditionalFeeStr = PriceFormatter.FormatPrice(paymentMethodAdditionalFee);

                    var par = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.PaymentMethodAdditionalFee"), paymentMethodAdditionalFeeStr), font);
                    par.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(par);
                }

                if (order.PointsEntry != null)
                {
                    string rpTitle = string.Format(_localizationService.GetResource("PDFInvoice.RewardPoints"), -order.PointsEntry.Points);
                    string rpAmount = PriceFormatter.FormatPrice(-(order.PointsEntry.UsedAmount));

                    var par = new Paragraph(String.Format("{0} {1}", rpTitle, rpAmount), font);
                    par.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(par);
                }

                string orderTotalStr = PriceFormatter.FormatPrice(order.OrderTotal);


                var pTotal = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.OrderTotal"), orderTotalStr), titleFont);
                pTotal.Alignment = Element.ALIGN_RIGHT;
                doc.Add(pTotal);

                #endregion

                #region Order notes

                if (_pdfSettings.RenderOrderNotes)
                {
                    var orderNotes = order.OrderNotes
                        .Where(on => on.DisplayToUser)
                        .OrderByDescending(on => on.CreateTime)
                        .ToList();
                    if (orderNotes.Count > 0)
                    {
                        doc.Add(new Paragraph(_localizationService.GetResource("PDFInvoice.OrderNotes"), titleFont));

                        doc.Add(new Paragraph(" "));

                        var notesTable = new PdfPTable(2);
                        notesTable.WidthPercentage = 100f;
                        notesTable.SetWidths(new[] { 30, 70 });

                        cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.OrderNotes.CreateTime"), font));
                        cell.BackgroundColor = BaseColor.LightGray;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        notesTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.OrderNotes.Note"), font));
                        cell.BackgroundColor = BaseColor.LightGray;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        notesTable.AddCell(cell);

                        foreach (var orderNote in orderNotes)
                        {
                            cell = new PdfPCell();
                            cell.AddElement(new Paragraph(orderNote.CreateTime.ToString(), font));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            notesTable.AddCell(cell);

                            cell = new PdfPCell();
                            cell.AddElement(new Paragraph(HtmlHelper.ConvertHtmlToPlainText(orderNote.FormatOrderNoteText(), true, true), font));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            notesTable.AddCell(cell);
                        }
                        doc.Add(notesTable);
                    }
                }

                #endregion

                ordNum++;
                if (ordNum < ordCount)
                {
                    doc.NewPage();
                }
            }
            doc.Close();
        }

        public virtual void PrintPackagingSlipsToPdf(Stream stream, IList<Shipment> shipments)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (shipments == null)
                throw new ArgumentNullException("shipments");

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.Letter;
            }

            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, stream);
            doc.Open();

            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.Black;
            var font = GetFont();
            var attributesFont = GetFont();
            attributesFont.SetStyle(Font.ITALIC);

            int shipmentCount = shipments.Count;
            int shipmentNum = 0;

            foreach (var shipment in shipments)
            {
                var order = shipment.Order;
                if (order.ShippingAddress != null)
                {
                    doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Shipment"), shipment.Id), titleFont));
                    doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Order"), order.Id), titleFont));

                    if (_addressSettings.CompanyEnabled && !String.IsNullOrEmpty(order.ShippingAddress.Company))
                        doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Company"), order.ShippingAddress.Company), font));

                    doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Name"), order.ShippingAddress.Name), font));
                    if (_addressSettings.PhoneEnabled)
                        doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Phone"), order.ShippingAddress.PhoneNumber), font));
                    if (_addressSettings.StreetAddressEnabled)
                        doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Address"), order.ShippingAddress.Address1), font));

                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(order.ShippingAddress.Address2))
                        doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Address2"), order.ShippingAddress.Address2), font));

                    if (_addressSettings.ProvincesEnabled || _addressSettings.ZipPostalCodeEnabled)
                        doc.Add(new Paragraph(String.Format("{0}, {1} {2}", order.ShippingAddress.ProvinceName,
                            order.ShippingAddress.CityName, order.ShippingAddress.AreaName), font));

                    doc.Add(new Paragraph(" "));

                    doc.Add(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.ShippingMethod"), order.ShippingMethod), font));
                    doc.Add(new Paragraph(" "));

                    var goodsTable = new PdfPTable(3);
                    goodsTable.WidthPercentage = 100f;
                    goodsTable.SetWidths(new[] { 60, 20, 20 });

                    var cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFPackagingSlip.GoodsName"), font));
                    cell.BackgroundColor = BaseColor.LightGray;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    goodsTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFPackagingSlip.SKU"), font));
                    cell.BackgroundColor = BaseColor.LightGray;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    goodsTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFPackagingSlip.QTY"), font));
                    cell.BackgroundColor = BaseColor.LightGray;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    goodsTable.AddCell(cell);

                    foreach (var sopv in shipment.ShipmentorderItems)
                    {
                        var opv = _orderService.GetOrderGoodsById(sopv.OrderItemId);
                        if (opv == null)
                            continue;

                        string name = opv.GoodsName;
                        cell = new PdfPCell();
                        cell.AddElement(new Paragraph(name, font));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(opv.AttributeDescription, true, true), attributesFont);
                        cell.AddElement(attributesParagraph);
                        goodsTable.AddCell(cell);

                        var sku = opv.Goods.FormatSku(opv.AttributesXml, _goodsSpecParser);
                        cell = new PdfPCell(new Phrase(sku ?? String.Empty, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        goodsTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(sopv.Quantity.ToString(), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        goodsTable.AddCell(cell);
                    }
                    doc.Add(goodsTable);
                }

                shipmentNum++;
                if (shipmentNum < shipmentCount)
                {
                    doc.NewPage();
                }
            }


            doc.Close();
        }

        public virtual void PrintGoodssToPdf(Stream stream, IList<Goods> list)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (list == null)
                throw new ArgumentNullException("list");

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.Letter;
            }

            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, stream);
            doc.Open();

            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.Black;
            var font = GetFont();

            int goodsNumber = 1;
            int prodCount = list.Count;

            foreach (var goods in list)
            {
                string goodsName = goods.Name;
                string goodsFullDescription = goods.FullDescription;

                doc.Add(new Paragraph(String.Format("{0}. {1}", goodsNumber, goodsName), titleFont));
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(HtmlHelper.StripTags(HtmlHelper.ConvertHtmlToPlainText(goodsFullDescription)), font));
                doc.Add(new Paragraph(" "));

                var pictures = _pictureService.GetPicturesByGoodsId(goods.Id);
                if (pictures.Count > 0)
                {
                    var table = new PdfPTable(2);
                    table.WidthPercentage = 100f;

                    for (int i = 0; i < pictures.Count; i++)
                    {
                        var pic = pictures[i];
                        if (pic != null)
                        {
                            var picBinary = _pictureService.LoadPictureBinary(pic);
                            if (picBinary != null && picBinary.Length > 0)
                            {
                                var pictureLocalPath = _pictureService.GetThumbLocalPath(pic, 200, false);
                                var cell = new PdfPCell(Image.GetInstance(pictureLocalPath));
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.Border = Rectangle.NO_BORDER;
                                table.AddCell(cell);
                            }
                        }
                    }

                    if (pictures.Count % 2 > 0)
                    {
                        var cell = new PdfPCell(new Phrase(" "));
                        cell.Border = Rectangle.NO_BORDER;
                        table.AddCell(cell);
                    }

                    doc.Add(table);
                    doc.Add(new Paragraph(" "));
                }

                #region variant

                doc.Add(new Paragraph(String.Format("{0}.{1}", goodsNumber, 1), font));
                doc.Add(new Paragraph(" "));

                doc.Add(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFGoodsCatalog.Price"), goods.Price.ToString("0.00")), font));
                doc.Add(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFGoodsCatalog.SKU"), goods.Sku), font));

                if (goods.IsShipEnabled && goods.Weight > Decimal.Zero)
                    doc.Add(new Paragraph(String.Format("{0}: {1} {2}", _localizationService.GetResource("PDFGoodsCatalog.Weight"), goods.Weight.ToString("0.00"), _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId).Name), font));

                if (goods.ManageStockEnabled)
                    doc.Add(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFGoodsCatalog.StockQuantity"), goods.StockQuantity), font));

                doc.Add(new Paragraph(" "));

                #endregion

                goodsNumber++;

                if (goodsNumber <= prodCount)
                {
                    doc.NewPage();
                }
            }

            doc.Close();
        }

        #endregion
    }
}