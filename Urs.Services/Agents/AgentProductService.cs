
using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Core;
using Urs.Core.Data;
using Urs.Data.Domain.Agents;

namespace Urs.Services.Agents
{
    public partial class AgentProductService : IAgentProductService
    {
        private readonly IRepository<AgentGoods> _repository;

        public AgentProductService(IRepository<AgentGoods> repository)
        {
            _repository = repository;
        }

        public virtual void Delete(AgentGoods agentproduct)
        {
            if (agentproduct == null)
                throw new ArgumentNullException("agentproduct");

            _repository.Delete(agentproduct);
        }

        public virtual AgentGoods GetById(int id)
        {
            if (id == 0)
                return null;

            return _repository.GetById(id);
        }

        public IPagedList<AgentGoods> GetAll(int pageIndex, int pageSize)
        {
            var query = from t in _repository.Table
                        select t;

            return new PagedList<AgentGoods>(query, pageIndex, pageSize);
        }

        public virtual IList<AgentGoods> GetListByProductIds(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return new List<AgentGoods>();

            var query = from t in _repository.Table
                        where ids.Contains(t.ProductId)
                        orderby t.Id descending
                        select t;

            var list = query.ToList();
            return list;
        }

        public virtual void Insert(AgentGoods entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            _repository.Insert(entity);
        }

        public virtual void Update(AgentGoods entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            _repository.Update(entity);
        }
    }
}
