﻿using System;
using System.Globalization;
namespace Urs.Services.Stores
{
    public class PriceFormatter
    {
        public static string FormatPrice(decimal price)
        {
            price = Math.Round(price, 2);

            return price.ToString("C", new CultureInfo("zh-CN"));
        }

    }
}
