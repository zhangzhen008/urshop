using Urs.Core;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    /// <summary>
    /// 海关接口
    /// </summary>
    public partial interface IShopService
    {
        #region Shop

        /// <summary>
        /// Deletes a customs shop
        /// </summary>
        /// <param name="shop">Customs Shop</param>
        void DeleteShop(Shop shop);
        /// <summary>
        /// Gets a customs shop
        /// </summary>
        /// <param name="shopId">The customs shop identifier</param>
        /// <returns>Shop</returns>
        Shop GetShopById(int shopId);
        /// <summary>
        /// 通过编号获取电商企业信息
        /// </summary>
        Shop GetShopByCode(string code);
        /// <summary>
        /// Gets all customs shop
        /// </summary>
        /// <returns>Customs Shops</returns>
        IPagedList<Shop> GetAllShop(int pageIndex = 0, int pageSize = int.MaxValue, bool? published = true);
        /// <summary>
        /// Inserts a customs shop
        /// </summary>
        /// <param name="shop">Customs Shop</param>
        void InsertShop(Shop shop);
        /// <summary>
        /// Updates the customs shop
        /// </summary>
        /// <param name="shop">Customs Shop</param>
        void UpdateShop(Shop shop);

        #endregion
    }
}
